# Ludum Dare 49
Ludum Dare 49 entry, Humble Homestead.

Playable at:
* https://tharbagroup.gitlab.io/ld49
* https://tharbakim.itch.io/humble-homestead

Ludum Dare entry at:
* https://ldjam.com/events/ludum-dare/49/humble-homestead

![](./coverimage.png)

The scope of the game got away from me a bit, and it was quite ambitious to start with.

The goal of the game is to build up as big of a population as you can - you start with only one member, so you're going to need to work your way up to converting one of your tiles into a camp in order to get started. Run out of food though and things will quickly take a turn for the worse - how far can you push the population in thus unstable economy?

## Controls
WASD, Arrow keys, and map border (inside UI frame) can all be used to move the map around.

1, 2, 3 control the speed at which a day elapses.

Space pauses/resumes the game when there are no other frames open over the map.

## Basic Mechanics
Resource management for three resources:
* Coins (used to buy land)
* Meat (used to feed population)
* Grass (used to feed animals)

### Grass
* Rabbits require 1 grass per day
* Chickens require 2 grass per day
* Cows require 5 grass per day

### Meat
All population members require 1 meat per day


## Win Condition
Technically the game is endless, but the maximum detected population size is 200 (controls the top left label, *Farm of LudumTown* in the cover image.

## Loss Condition
If you don't have enough food to feed your population, your population will die off or move on to other more inhabitable settlements. When your population hits 0 you lose.

# Post-Release Changelog

* Fixed animal menu not displaying correctly when more animals than screen space were present.
* Increased starting population to 2, to work around flaw in the population growth calculation.
* Made the population growth apply to the population count, and fixed the comparison that generates the message.
* Fixed call to `voidToGrass` having displaced arguments after the map had been adjusted for a purchase on the top or left border, resulting in a broken map.
* Fix title not being set on purchased tiles
* Fixed sloppy calls to `Math.random()` being done incorrectly. 
* Allow land purchases with exact currency
* Label camp tiles correctly after grass tile is converted
* Fix a random number in a random event being off by 1
* Fix crash when random event spawning non-chicken occurred due to not importing all possible animal classes
* Fix animal deaths not being calculated or applied correctly
* Fix town size not being detected correctly
* Fixed deep copying issue with map expansion
* Fixed animals gained by random event not having default values
* Fixed camp tiles throwing an error when processing a purchase
* Fixed not being able to buy land with exact money

* Balance changes

# Known Issues
Viewing a land tile and then returning to the game triggers a reload of the game map (intended), and resets the time on the day (unintended)