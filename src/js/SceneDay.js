
export let SceneDay = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneDay" })

        let resize = () => {
            if (this.sys.game.scene.isActive(this.scene.key)) {
                this.scene.restart()
            }
        }

        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function (data) {
        this.gameState = data.gameState
        this.gameSettings = data.gameSettings
        this.gameMap = data.gameMap
    },
    preload: function () {
        this.load.spritesheet('animals', 'images/animals.png', {
            frameWidth: 32,
            frameHeight: 32
        })
        this.load.spritesheet('tiles', 'images/tiles.png', {
            frameWidth: 32,
            frameHeight: 32
        })
        this.load.spritesheet('currencies', 'images/currencies.png', {
            frameWidth: 32,
            frameHeight: 32
        })
        this.load.image('skybox', 'images/background.png')
        this.load.image('saleSign', 'images/sale.png')

    },
    create: function () {


        const sceneUI = this.scene.get('SceneUI')
        const background = this.scene.launch('SceneBackground')
        this.scene.sendToBack('SceneBackground')

        const defaultZoom = 4
        /**
         * Set camera so we can calculate width based on canvas * zoom, then set it again
         */
        this.cameras.main.setBounds(0, 0, 0, 0)
        this.cameras.main.setZoom(4)
        const maxWidth = (this.gameMap.getWidth() * this.cameras.main.zoom) + (this.cameras.main.displayWidth)
        const maxHeight = (this.gameMap.getHeight() * this.cameras.main.zoom) + (this.cameras.main.displayHeight)
        const minWidth = -this.cameras.main.displayWidth
        const minHeight = - this.cameras.main.displayHeight
        this.cameras.main.setBounds(minWidth, minHeight, maxWidth, maxHeight)

        this.cameras.main.centerOn((this.gameMap.getWidth()), (this.gameMap.getHeight()))


        this.gameMap.draw(this)


        this.scene.launch('SceneUI', this.gameState)
        this.scene.bringToTop('SceneUI')


        this.hourLoop = this.time.addEvent({
            delay: 3000,
            loop: true,
            callback: () => {
                this.gameState.hour++
                if (this.gameState.hour < 3) {
                    //Morning
                    this.events.emit('updateTime', 'Morning')
                } else if (this.gameState.hour < 9) {
                    //Day
                    this.events.emit('updateTime', 'Day')
                } else if (this.gameState.hour < 15) {
                    //Evening
                    this.events.emit('updateTime', 'Evening')
                } else {
                    //Night
                    this.events.emit('updateTime', 'Night')
                }
                if (this.gameState.hour > 16) {
                    this.gameState.day++
                    this.gameState.hour = 1
                    this.cameras.main.fadeOut(1000, 0, 0, 0)
                    this.scene.stop('SceneBackground')
                    this.scene.stop('SceneUI')
                    this.scene.start("SceneManageDay", { gameState: this.gameState, gameSettings: this.gameSettings, gameMap: this.gameMap })
                }

            }
        })

        this.input.keyboard.on('keydown-SPACE', () => {
            if (this.time.paused !== true) {
                this.time.timeScale = 0
                this.events.emit('updateTimeLoop', this.time.timeScale)
            }
        })

        this.input.keyboard.on('keydown-ONE', () => {
            if (this.time.paused !== true) {
                this.time.timeScale = 1
                this.events.emit('updateTimeLoop', this.time.timeScale)
            }
        })
        this.input.keyboard.on('keydown-TWO', () => {
            if (this.time.paused !== true) {
                this.time.timeScale = 2
                this.events.emit('updateTimeLoop', this.time.timeScale)
            }
        })
        this.input.keyboard.on('keydown-THREE', () => {
            if (this.time.paused !== true) {
                this.time.timeScale = 3
                this.events.emit('updateTimeLoop', this.time.timeScale)
            }
        })
        sceneUI.events.on('pause', () => {
            this.time.paused = true
        })

        sceneUI.events.on('resume', () => {
            this.time.paused = false
        })

        this.cameras.main.fadeIn(1000, 0, 0, 0)
    },
    update: function () {
        let keyState = this.input.keyboard.addKeys('up,left,down,right,W,A,S,D')
        if ((this.input.activePointer.x < this.cameras.main.centerX - this.cameras.main.width / 2 + 100 &&
            this.input.activePointer.x > this.cameras.main.centerX - this.cameras.main.width / 2 + 50) || keyState.left.isDown || keyState.A.isDown) {
            //Scroll left
            this.cameras.main.scrollX -= 5
        } else if ((this.input.activePointer.x > this.cameras.main.centerX + this.cameras.main.width / 2 - 100 &&
            this.input.activePointer.x < this.cameras.main.centerX + this.cameras.main.width / 2 - 50) || keyState.right.isDown || keyState.D.isDown) {
            //Scroll Right
            this.cameras.main.scrollX += 5
        }

        if ((this.input.activePointer.y < this.cameras.main.centerY - this.cameras.main.height / 2 + 100 &&
            this.input.activePointer.y > this.cameras.main.centerY - this.cameras.main.height / 2 + 50) || keyState.up.isDown || keyState.W.isDown) {
            //Scroll Up
            this.cameras.main.scrollY -= 5
        } else if ((this.input.activePointer.y > this.cameras.main.centerY + this.cameras.main.height / 2 - 100 &&
            this.input.activePointer.y < this.cameras.main.centerY + this.cameras.main.height / 2 - 50) || keyState.down.isDown || keyState.S.isDown) {
            //Scroll Down
            this.cameras.main.scrollY += 5
        }

    },
})