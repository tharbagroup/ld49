
export let SceneBackground = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneBackground" })
        let resize = () => {
            if (this.sys.game.scene.isActive(this.scene.key)) {
                this.scene.restart()
            }
        }

        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function () {
    },
    preload: function () {
        this.load.image('skybox', 'images/background.png')
    },
    create: function () {


        let background = this.add.image(this.cameras.main.displayWidth / 2, this.cameras.main.displayHeight / 2, 'skybox')
            .setScrollFactor(0, 0)
            .setDepth(-1)
        background.displayWidth = this.cameras.main.displayWidth
        background.displayHeight = this.cameras.main.displayHeight

    },
    update: function () { }
})