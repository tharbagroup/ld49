import { Animal } from "./Animal.js";

export class Cow extends Animal{
    


    constructor(properties = {})
    {
        super()
        this.possibleAnimalNames = ['Bessy']
        this.produceChance = Math.floor(Math.random() * 100 + 50)
        this.produceName = "unit of milk"
        this.produceNamePlural = "units of milk"
        this.produceMultiplier = 2
        this.expectedAge = 22
        this.birthChance = 5
        this.texture = 1
        this.basePrice = 100
        this.baseMeat = 50
        this.baseEat = 5

        this.loadProperties(properties)   
    }

    newBirth = () => {
        return new Cow()
    }

}