import { Animal } from "./Animal.js";

export class Rabbit extends Animal{
    


    constructor(properties = {})
    {
        super()
        this.possibleAnimalNames = ['Hopper', 'Easter']
        this.produceChance = 0
        this.produceName = "Egg"
        this.produceNamePlural = "Eggs"
        this.expectedAge = 5
        this.birthChance = 25
        this.texture = 2
        this.basePrice = 2
        this.baseMeat = 2
        this.baseEat = 1

        this.loadProperties(properties)   
    }

    newBirth = () => {
        return new Rabbit()
    }

}