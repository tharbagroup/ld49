export class Animal {
    static animalNames = [
        'Bob',
        'Carl',
        'Gary',
        'Ryan',
        'Jose',
        'Bailey',
        'Max',
        'Charlie',
        'Riley',
        'Duke',
        'Phil',
        'Neil',
        'Wes'
        ,
        'Wesley',
        'Chris',
        'Alyssa',
        'Isabella',
        'Teddy',
        'Victor',
        'Sean',
        'Manny',
        'Turner',
        'Jack',
        'Zoe',
        'Sam',
        'Sasha',
        'Annie'
    ]

    constructor(properties = {}) {
        this.properties = properties
    }



    generateName = () => {
        let possibleNames
        if ('possibleAnimalNames' in this) {
            possibleNames = Animal.animalNames.concat(this.possibleAnimalNames)
        } else {
            possibleNames = Animal.animalNames
        }
        return possibleNames[Math.floor(Math.random() * possibleNames.length)]
    }

    loadProperties = (properties) => {
        for (const [key, value] of Object.entries(properties)) {
            this[key] = value
        }

        if (!('animalName' in this)) {
            this.animalName = this.generateName()
        }
        if (!('animalAge' in this)) {
            this.animalAge = 1
        }

        this.dead = false
    }

    chanceDeath = (feedCount = 0) => {
        let deathConstant
        if (feedCount < 1) {
            deathConstant = 30
        } else {
            deathConstant = 10
        }
        const chance = (deathConstant * (this.animalAge / this.expectedAge))
        if (Math.floor(Math.random() * 100) < chance) {
            //Animal has died.
            this.dead = true
            return 1
        } else {
            return 0
        }

    }

    chanceProduce = () => {
        let produce = 0
        let chance = this.produceChance
        if (chance > 0) {
            while (Math.floor(Math.random() * 100) > 50) {
                produce++
                chance -= 50
            }
        }
        this.lastProduce = produce
        return produce
    }

    chanceBirth = () => {
        const chance = 100 - this.birthChance
        if (Math.floor(Math.random() * 100) > chance) {
            //Animal has been born
            const animal = this.newBirth()
            return true
        }
        return false
    }

    age = () => {
        this.animalAge++

    }

    calculatePrice = () => {
        this.price = this.basePrice
    }
    calculateMeat = () => {
        this.meat = this.baseMeat
    }
    eat = () => {
        return this.baseEat
    }
}