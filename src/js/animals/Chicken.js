import { Animal } from "./Animal.js";

export class Chicken extends Animal{
    


    constructor(properties = {})
    {
        super()
        this.possibleAnimalNames = ['Ernest']
        this.produceChance = Math.floor(Math.random() * 100 + 50)
        this.produceName = "Egg"
        this.produceNamePlural = "Eggs"
        this.produceMultiplier = 1
        this.expectedAge = 12
        this.birthChance = 15
        this.texture = 0
        this.basePrice = 10
        this.baseMeat = 5
        this.baseEat = 2

        this.loadProperties(properties)   
    }

    newBirth = () => {
        return new Chicken()
    }

}