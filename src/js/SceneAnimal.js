export let SceneAnimal = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneAnimal" })
        let resize = () => {
            try {
            if (this.sys.game.scene.isActive(this.scene.key)) {
                this.scene.restart()
            }
        } catch (e) {}
        }


        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function (data) {
        this.gameState = data.gameState
        this.animal = data.animal
    },
    preload: function() {
        this.load.spritesheet('uiBorder', 'images/ui-border.png', {
            frameWidth: 32,
            frameHeight: 32    
        })  
    },
    create: function () {
        const background = this.add.graphics()
        background.fillStyle(0xCCCCCC, 0.7)

        background.fillRoundedRect(100, 100, this.sys.game.canvas.width -200, this.sys.game.canvas.height - 200, 32)
        background.setDepth(20)
        background.setScrollFactor(0)

        let listStartHeight = 116
        try {
        this.gameState.animals[this.animal].forEach((animal, index, animals) => {
        const animalBackground = this.add.graphics()
        animalBackground.fillStyle(0xFFFFFF, 0.9)

        animalBackground.fillRoundedRect(116, listStartHeight, this.sys.game.canvas.width -232, 64, 4)
        animalBackground.setDepth(21)
        animalBackground.setScrollFactor(0)
            let animalInset = 132
        const animalIcon = this.add.sprite(animalInset, listStartHeight + 32, 'animals', animal.texture)
            .setDepth(22)
            .setOrigin(0.5, 0.5)
            .setScrollFactor(0, 0)

            animalInset += 32
        const animalName = this.add.text(animalInset, listStartHeight + 32, `${animal.animalName}`, {
            fontSize: 32,
            color: "#000000",
            fontStyle: "bold",
        }).setDepth(22)
          .setOrigin(0, 0.5)
          .setScrollFactor(0, 0)

          animalInset += animalName.displayWidth + 16

        if (animalInset < this.sys.game.canvas.width - 300) {
            const animalAge = this.add.text(animalInset, listStartHeight + 32, `Age:(${animal.animalAge})`, {
                fontSize: 32,
                color: "#777777",
                fontStyle: "bold",
            }).setDepth(22)
              .setOrigin(0, 0.5)
              .setScrollFactor(0, 0)
    
              animalInset += animalAge.displayWidth + 16
        }

        if (animalInset < this.sys.game.canvas.width - 300) {
            const animalProduce = this.add.text(animalInset, listStartHeight + 32, `Yesterday's Produce:(${animal.lastProduce})`, {
                fontSize: 32,
                color: "#777777",
                fontStyle: "bold",
            }).setDepth(22)
              .setOrigin(0, 0.5)
              .setScrollFactor(0, 0)
    
              animalInset += animalProduce.displayWidth + 16
        }
        const animalSell = this.add.text(this.sys.game.canvas.width - 132, listStartHeight + 32, `${animal.price}`, {
                fontSize: 32,
                color: "#777777",
                fontStyle: "bold",
                backgroundColor: "#88ff88",
                padding: { left: 37, right: 5,  y: 5 }
            }).setDepth(22)
              .setOrigin(1, 0.5)
              .setScrollFactor(0, 0)
              .setInteractive({ useHandCursor: true })
              .on('pointerdown', () => {
                  this.gameState.money += animal.price
                  animals.splice(index, 1)
                  this.events.emit('updateCurrency')
                  this.events.emit('updateAnimals')
                  this.scene.restart()
              })


        const animalSellIcon = this.add.sprite(this.game.canvas.width - 132 - (animalSell.displayWidth),listStartHeight + 32, 'currencies', '0')
          .setDepth(23)
          .setOrigin(0, 0.5)
          .setScrollFactor(0, 0)

              const animalMeat = this.add.text(this.sys.game.canvas.width - 140 - animalSell.displayWidth, listStartHeight + 32, `${animal.meat}`, {
                fontSize: 32,
                color: "#777777",
                fontStyle: "bold",
                backgroundColor: "#ff8888",
                padding: { left: 37, right: 5,  y: 5 }
            }).setDepth(22)
              .setOrigin(1, 0.5)
              .setScrollFactor(0, 0)
              .setInteractive({ useHandCursor: true })
              .on('pointerdown', () => {
                this.gameState.meat += animal.meat
                animals.splice(index, 1)
                this.events.emit('updateCurrency')
                this.events.emit('updateAnimals')
                this.scene.restart()
            })

              const animalMeatIcon = this.add.sprite(this.game.canvas.width - 140 - animalSell.displayWidth - (animalMeat.displayWidth),listStartHeight + 32, 'currencies', '1')
              .setDepth(23)
              .setOrigin(0, 0.5)
              .setScrollFactor(0, 0)


        listStartHeight += 72
            if (listStartHeight + 64 > (this.sys.game.canvas.height - 250)) {
                throw RangeError
            }
        })
    
    } catch (e) {} 
        let saveButton = this.add.text(
            this.sys.game.canvas.width / 2,
            this.sys.game.canvas.height - 150,
            "Return to Game",
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold",
                backgroundColor: "#88ff88",
                padding: { x: 10, y: 10 },

            }
        )
            .setOrigin(0.5)
            .setInteractive({ useHandCursor: true })
            .setDepth(21)
            .on('pointerdown', () => {
                this.events.emit('close')
                this.gameState.paused = false
            })
    },
    update: function () {

    }
})