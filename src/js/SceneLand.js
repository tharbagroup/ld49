import { GameMap } from "./GameMap.js";

export let SceneLand = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneLand" })
        let resize = () => {
            try {
            if (this.sys.game.scene.isActive(this.scene.key)) {
                this.scene.restart()
            }
        } catch (e) {}
        }

        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function (data) {
        this.gameState = data.gameState
        this.row = data.row
        this.column = data.column
        this.gameMap = data.gameMap
    },
    preload: function () {
        this.load.spritesheet('uiBorder', 'images/ui-border.png', {
            frameWidth: 32,
            frameHeight: 32
        })

        this.load.image('saleSign', 'images/sale.png')
    },
    create: function () {
        const background = this.add.graphics()
        background.fillStyle(0xCCCCCC, 0.7)

        background.fillRoundedRect(100, 100, this.sys.game.canvas.width - 200, this.sys.game.canvas.height - 200, 32)
        background.setDepth(20)
        background.setScrollFactor(0)

        let listStartHeight = 116
        const landBackground = this.add.graphics()
        landBackground.fillStyle(0xFFFFFF, 0.9)

        landBackground.fillRoundedRect(116, listStartHeight, this.sys.game.canvas.width - 232, 480, 4)
        landBackground.setDepth(21)
        landBackground.setScrollFactor(0)
        let landInset = 132
        listStartHeight += 16

        const landIcon = this.add.sprite(this.sys.game.canvas.width / 2, listStartHeight, 'tiles', this.gameMap.map[this.row][this.column].texture)
            .setDepth(22)
            .setOrigin(0.5, 0)
            .setScrollFactor(0, 0)
            .setScale(4)

        if (this.gameMap.map[this.row][this.column].owned !== true) {
            let sign = this.add.sprite(this.sys.game.canvas.width / 2, listStartHeight + (landIcon.displayHeight / 2), 'saleSign')
                .setDepth(23)
                .setOrigin(0.5, 0.5)
                .setScale(2)
                .setScrollFactor(0, 0)
        }
        listStartHeight += landIcon.displayHeight + 8


        const landIconText = this.add.text(this.sys.game.canvas.width / 2, listStartHeight, `${this.gameMap.map[this.row][this.column].title}`, {
            fontSize: 32,
            color: "#000000",
            fontStyle: "bold",
        }
        ).setDepth(22)
            .setOrigin(0.5, 0)
            .setScrollFactor(0, 0)


        listStartHeight += 72

        if (this.gameMap.map[this.row][this.column].owned !== true) {
            /**Calculate Price */
            let price = 50
            price += (Math.abs(this.column - this.gameMap.getTilesX() / 2) * 5)
            price += (Math.abs(this.row - this.gameMap.getTilesY() / 2) * 5)
            price = Math.ceil(price)
            /** Buy option */
            let backgroundColor
            if (this.gameState.money > price) {
                backgroundColor = "#88ff88"
            } else {
                backgroundColor = "#777777"
            }

            let buyButton = this.add.text(
                this.sys.game.canvas.width / 2,
                listStartHeight,
                `$${price}`,
                {
                    fontSize: 32,
                    color: "#FFFFFF",
                    fontStyle: "bold",
                    backgroundColor: backgroundColor,
                    padding: { x: 10, y: 10 },

                }
            )
            .setOrigin(0.5)
            .setDepth(22)


            let buyButtonText = this.add.text(
                this.sys.game.canvas.width / 2,
                listStartHeight + buyButton.displayHeight,
                `+2 Grass/Day`,
                {
                    fontSize: 24,
                    color: "#888888",
                    fontStyle: "bold"
                }

            )
                .setOrigin(0.5)
                .setDepth(22)

            if (this.gameState.money >= price) {
                buyButton.setInteractive({ useHandCursor: true })
                buyButton.on('pointerdown', () => {
                    this.gameMap.map[this.row][this.column].owned = true
                    this.gameState.money -= price
                    this.gameMap.edgeCheck(this.row, this.column)
                    this.events.emit('close')
                })
            }
        } else {
            /** Grass to Camp */
            if (this.gameMap.map[this.row][this.column].texture === GameMap.TILE_GRASS) {
            /**Calculate Price */
            let price = 100
            price += (Math.abs(this.column - this.gameMap.getTilesX() / 2) * 5)
            price += (Math.abs(this.row - this.gameMap.getTilesY() / 2) * 5)
            price = Math.ceil(price)
            /** Buy option */
            let backgroundColor
            if (this.gameState.money > price) {
                backgroundColor = "#88ff88"
            } else {
                backgroundColor = "#777777"
            }

            let upgradeButton = this.add.text(
                this.sys.game.canvas.width / 2,
                listStartHeight,
                `Convert to Camp ($${price})`,
                {
                    fontSize: 32,
                    color: "#FFFFFF",
                    fontStyle: "bold",
                    backgroundColor: backgroundColor,
                    padding: { x: 10, y: 10 },

                }
            )
                .setOrigin(0.5)
                .setDepth(22)

                let upgradeButtonText = this.add.text(
                    this.sys.game.canvas.width / 2,
                    listStartHeight + upgradeButton.displayHeight,
                    `+2% Population Growth Chance/Day`,
                    {
                        fontSize: 24,
                        color: "#888888",
                        fontStyle: "bold"
                    }
    
                )
                    .setOrigin(0.5)
                    .setDepth(22)


            if (this.gameState.money >= price) {
                upgradeButton.setInteractive({ useHandCursor: true })
                upgradeButton.on('pointerdown', () => {
                    this.gameMap.map[this.row][this.column].texture = GameMap.TILE_CAMP
                    this.gameMap.map[this.row][this.column].title = "Camp Tile"
                    this.gameState.money -= price
                    this.events.emit('close')
                })
            }
        }

        }
        let saveButton = this.add.text(
            this.sys.game.canvas.width / 2,
            this.sys.game.canvas.height - 150,
            "Return to Game",
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold",
                backgroundColor: "#88ff88",
                padding: { x: 10, y: 10 },

            }
        )
            .setOrigin(0.5)
            .setInteractive({ useHandCursor: true })
            .setDepth(21)
            .on('pointerdown', () => {
                this.events.emit('close')
                this.gameState.paused = false
            })
    },
    update: function () {

    }
})