import { Chicken } from "./animals/Chicken.js"
import { Cow } from "./animals/Cow.js"
import { Rabbit } from "./animals/Rabbit.js"
import { GameMap } from "./GameMap.js"

export let SceneManageDay = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneManageDay" })
        let resize = () => {
            if (this.sys.game.scene.isActive(this.scene.key)) {
                this.scene.restart({ gameState: this.gameState, gameSettings: this.gameSettings, gameMap: this.gameMap })
            }
        }

        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function (data) {
        this.gameState = data.gameState
        this.gameSettings = data.gameSettings
        if (!(Object.prototype.hasOwnProperty.call(data, 'gameMap'))) {
            this.gameMap = new GameMap(this.gameState)
        } else {
            this.gameMap = data.gameMap
        }
    },
    preload: () => { },
    create: function () {
        let messages = []
        if (this.gameState.processedDay < this.gameState.day) {
            /**
             * Safety check to prevent rerolling day by resizing the window
             */
            let deaths = 0
            let births = 0
            let produce = 0
            let starving = 0
            let produceTypes = {}
            let messageArray = []
            for (const [type, animals] of Object.entries(this.gameState.animals)) {
                let animalIndex = 0
                animals.forEach(animal => {
                    /**
                     * Nightly animal processes
                     */
                    const foodRequired = animal.eat()
                    animal.age()
                    if (this.gameState.grass <= foodRequired) {
                        this.gameState.grass = 0
                        starving += 1
                    } else {
                        this.gameState.grass -= foodRequired
                    }
                    if (this.gameState.day !== 1) {
                        deaths += animal.chanceDeath(this.gameState.grass)
                        if (animal.dead === true) {
                            this.gameState.animals[type].splice(animalIndex, 1)
                        }

                    }
                    if (this.gameState.animals[type].length > 1) {
                        if (animal.chanceBirth()) {
                            births++
                            let newBirth = animal.newBirth()

                            newBirth.calculatePrice()
                            newBirth.calculateMeat()
                            newBirth.lastProduce = 0
                            this.gameState.animals[newBirth.constructor.name].push(newBirth)
                        }
                    }
                    const produceCount = animal.chanceProduce()
                    produce += produceCount
                    this.gameState.meat += produce
                    if (!(Object.prototype.hasOwnProperty.call(produceTypes, animal.constructor.name))) {
                        produceTypes[animal.constructor.name] = { count: produceCount, produceName: animal.produceName, produceNamePlural: animal.produceNamePlural, produceMultiplier: animal.produceMultiplier }
                    } else {
                        produceTypes[animal.constructor.name].count += produceCount
                    }
                    animal.calculatePrice()
                    animal.calculateMeat()
                    animalIndex++
                })
            }

            /**
             * Land Processing
             */
            let populationGrowChance = 2
            for (const row of this.gameMap.map) {
                for (const cell of row) {
                    if (cell.owned === true) {
                        if (cell.texture === GameMap.TILE_GRASS) {
                            this.gameState.grass += 2
                        } else if (cell.texture === GameMap.TILE_CAMP) {
                            populationGrowChance += 2
                        }

                    }
                }
            }
            if (this.gameState.meat < this.gameState.population) {
                this.gameState.population = this.gameState.meat
                this.gameState.meat = 0
            } else {
                this.gameState.meat -= this.gameState.population
            }
            if (this.gameState.population === 0) {
                /**
                 * Game over!
                 */
                alert(`Everybody in your settlement has died or left! You survived ${this.gameState.day} days, play again?`)
                this.scene.start('SceneMenu')

            }

            /**
             * Population Processing
             */
            let newPop = 0
            if (this.gameState.population > 1) {
                while (Math.floor(Math.random() * 100) < populationGrowChance) {
                    newPop++
                    populationGrowChance = populationGrowChance / 2
                    /** Population grow! */
                }
                if (newPop === 1) {
                    messages.push("A new member of your population has been born.")
                    this.gameState.population += newPop

                } else if (newPop > 1) {

                    messages.push(`${newPop} new members of your population have been born.`)
                    this.gameState.population += newPop
                }
            }

            /**
             * Random Event Processing
             */

            /**
             * Gain a population
             */
            if (Math.floor(Math.random() * 100) < this.gameState.recruitChance) {
                this.gameState.population++
                messages.push('A settler looking for a new beginning has joined you!')
            }

            /**
             * Gain a random animal
             */
            if (Math.floor(Math.random() * 100) < this.gameState.animalChance) {
                const randomAnimal = Math.floor(Math.random() * 3)
                let newAnimal
                if (randomAnimal === 0) {
                    newAnimal = new Chicken({ animalAge: Math.floor(Math.random() * 10) })
                    newAnimal.calculatePrice()
                    newAnimal.calculateMeat()
                    newAnimal.lastProduce = 0
                    this.gameState.animals.Chicken.push(newAnimal)

                    newAnimal = new Chicken({ animalAge: Math.floor(Math.random() * 10) })
                    newAnimal.calculatePrice()
                    newAnimal.calculateMeat()
                    newAnimal.lastProduce = 0
                    this.gameState.animals.Chicken.push(newAnimal)
                    messages.push('A pair of chickens have wandered onto your land!')
                } else if (randomAnimal === 1) {
                    newAnimal = new Cow({ animalAge: Math.floor(Math.random() * 15) })
                    newAnimal.calculatePrice()
                    newAnimal.calculateMeat()
                    newAnimal.lastProduce = 0
                    this.gameState.animals.Cow.push(newAnimal)
                    newAnimal = new Cow({ animalAge: Math.floor(Math.random() * 15) })
                    newAnimal.calculatePrice()
                    newAnimal.calculateMeat()
                    newAnimal.lastProduce = 0
                    this.gameState.animals.Cow.push(newAnimal)
                    messages.push('A pair of cows have wandered onto your land!')
                } else if (randomAnimal === 2) {
                    newAnimal = new Rabbit({ animalAge: Math.floor(Math.random() * 5) })
                    newAnimal.calculatePrice()
                    newAnimal.calculateMeat()
                    newAnimal.lastProduce = 0
                    this.gameState.animals.Rabbit.push(newAnimal)
                    newAnimal = new Rabbit({ animalAge: Math.floor(Math.random() * 5) })
                    newAnimal.calculatePrice()
                    newAnimal.calculateMeat()
                    newAnimal.lastProduce = 0
                    this.gameState.animals.Rabbit.push(newAnimal)
                    messages.push('A pair of rabbits have wandered onto your land!')
                } else {
                    messages.push('Fresh animal tracks were spotted nearby! Strange..')
                }
            }

            if (deaths === 1) {
                messages.push(`${deaths} animal has died.`)
            } else if (deaths > 1) {
                messages.push(`${deaths} animals have died.`)
            }

            if (births === 1) {
                messages.push(`${births} animal has been born.`)
            } else if (births > 1) {
                messages.push(`${births} animals have been born.`)
            }


            if (produce > 1) {
                if (produce === 1) {

                    messageArray = [`Your animals have produced ${produce} item.`]
                } else {
                    messageArray = [`Your animals have produced ${produce} items.`]
                }
                for (const produceType in produceTypes) {
                    if (produceTypes[produceType].count > 0) {
                        let produceName
                        if (produceTypes[produceType].count === 1) {
                            produceName = produceTypes[produceType].produceName
                        } else {
                            produceName = produceTypes[produceType].produceNamePlural
                        }
                        messageArray.push(`${produceTypes[produceType].count} ${produceName}, + ${produceTypes[produceType].count * produceTypes[produceType].produceMultiplier} Meat`)
                    }
                }
                messages.push(messageArray)
            }

            if (starving > 0) {
                messages.push(`${starving} of your animals did not get enough to eat.`)
            }

            if (messages.length === 0) {
                messages.push('Nothing eventful has occured.')
            }
            this.gameState.messages = messages
            this.gameState.processedDay++
        } else {
            messages = this.gameState.messages
        }
        let i = 0

        const menuText = this.add.text(
            this.sys.game.canvas.width / 2,
            0 + 16,
            `Morning, Day ${this.gameState.day}`,
            {
                fontSize: 32,
                color: "#FF0000",
                fontStyle: "bold"
            }
        ).setOrigin(0.5, 0)

        const printMessage = (messageLine, skipSpace = false) => {
            if (Array.isArray(messageLine)) {
                messageLine.forEach(line => {
                    printMessage(line, true)
                })
                i++
            } else {
                this.add.text(
                    this.sys.game.canvas.width / 2,
                    this.sys.game.canvas.height / 4 + (32 * i),
                    messageLine,
                    {
                        fontSize: 32,
                        color: "#FF0000",
                        fontStyle: "bold"
                    }
                ).setOrigin(0.5, 0)
                i++
                if (skipSpace !== true) {
                    i++

                }

            }
        }

        messages.forEach(message => {
            printMessage(message)


        })




        let startButton = this.add.text(
            this.sys.game.canvas.width / 2,
            this.sys.game.canvas.height - 48,
            "Continue",
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold",
                backgroundColor: "#88ff88",
                padding: { x: 10, y: 10 },

            }
        )
            .setOrigin(0.5)
            .setInteractive({ useHandCursor: true })
            .on('pointerdown', () => { this.scene.start("SceneDay", { gameState: this.gameState, gameSetting: this.gameSettings, gameMap: this.gameMap }) })

        this.cameras.main.fadeIn(1000, 0, 0, 0)
    },
    update: () => { }
})