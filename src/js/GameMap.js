import { SceneLand } from './SceneLand.js'

export class GameMap {
    static DEFAULT_ROWS = 4
    static DEFAULT_COLUMNS = 4

    static TILE_HEIGHT = 32
    static TILE_WIDTH = 32
    static TILE_SCALE = 2

    static TILE_VOID = 0
    static TILE_GRASS = 1
    static TILE_CAMP = 2
    constructor(gameState) {
        this.sceneLandMenu
        this.gameState = gameState
        this.map = []
        for (let i = 0; i < GameMap.DEFAULT_ROWS; i++) {
            this.map[i] = []
            for (let j = 0; j < GameMap.DEFAULT_COLUMNS; j++) {
                this.map[i][j] = { texture: GameMap.TILE_GRASS, owned: false, title: "Grass Tile" }
            }
        }
        this.map[1][1].owned = true
        this.map[1][2].owned = true
        this.map[2][1].owned = true
        this.map[2][2].owned = true

        this.map[0][0] = { texture: GameMap.TILE_VOID, owned: false }
        this.map[0][3] = { texture: GameMap.TILE_VOID, owned: false }
        this.map[3][0] = { texture: GameMap.TILE_VOID, owned: false }
        this.map[3][3] = { texture: GameMap.TILE_VOID, owned: false }
    }



    draw = (game) => {


        const clearLandMenu = () => {
            this.sceneLandMenu.events.off('close')
            this.sceneLandMenu.events.off('updateCurrency')
            game.scene.remove('SceneLand')
            this.sceneLandMenu = null
            game.gameState.paused = false
            game.time.paused = false
            game.scene.restart()
            game.events.emit('updateTimeLoop', game.time.timeScale)
        }

        /**
         * These should be traditional for loops instead of forEach
         */




        let rowId = 0
        this.tileMap = []
        this.map.forEach(row => {
            let columnId = 0
            row.forEach(column => {
                if (!(Array.isArray(this.tileMap[rowId]))) {
                    this.tileMap[rowId] = []
                }
                this.tileMap[rowId][columnId] = game.add.sprite((columnId * (GameMap.TILE_WIDTH * GameMap.TILE_SCALE)) + (16 * GameMap.TILE_SCALE), (rowId * (GameMap.TILE_HEIGHT * GameMap.TILE_SCALE)) + (16 * GameMap.TILE_SCALE), 'tiles', this.map[rowId][columnId].texture)
                this.tileMap[rowId][columnId].scale = GameMap.TILE_SCALE
                this.tileMap[rowId][columnId].textureValue = this.map[rowId][columnId].texture
                if (this.map[rowId][columnId].texture !== 0 && this.map[rowId][columnId].owned !== true) {
                    let sign = game.add.sprite((columnId * (GameMap.TILE_WIDTH * GameMap.TILE_SCALE)) + (16 * GameMap.TILE_SCALE), (rowId * (GameMap.TILE_HEIGHT * GameMap.TILE_SCALE)) + (16 * GameMap.TILE_SCALE), 'saleSign')
                    sign.setDepth(2)
                }
                this.tileMap[rowId][columnId].row = rowId
                this.tileMap[rowId][columnId].column = columnId
                this.tileMap[rowId][columnId].setInteractive()
                this.tileMap[rowId][columnId].setDepth(1)
                this.tileMap[rowId][columnId].on('pointerover', function (pointer) {
                    this.setTint(0xcccccc)
                })
                this.tileMap[rowId][columnId].on('pointerout', function (pointer) {
                    this.clearTint()
                })
                let that = this
                this.tileMap[rowId][columnId].on('pointerdown', function () {
                    if (game.gameState.paused !== true && this.textureValue !== GameMap.TILE_VOID) {
                        game.gameState.paused = true
                        game.time.paused = true
                        game.events.emit('updateTimeLoop', 0)
                        game.scene.add('SceneLand', SceneLand)
                        game.scene.launch('SceneLand', { gameState: game.gameState, row: this.row, column: this.column, gameMap: that })
                        that.sceneLandMenu = game.scene.get('SceneLand')
                        that.sceneLandMenu.events.on('close', () => {
                            clearLandMenu()
                        })
                        that.sceneLandMenu.events.on('updateCurrency', () => {
                            game.events.emit('updateCurrency')
                        })
                        game.scene.bringToTop('SceneLand')
                    }
                })
                columnId++
            })
            rowId++
        })

    }

    getWidth = () => {
        let maxWidth = 0
        this.map.forEach(row => {
            if (row.length > maxWidth) {
                maxWidth = row.length
            }
        })
        return maxWidth * GameMap.TILE_WIDTH
    }

    getTilesX = () => {
        let maxWidth = 0
        this.map.forEach(row => {
            if (row.length > maxWidth) {
                maxWidth = row.length
            }
        })
        return maxWidth
    }

    getTilesY = () => {
        return this.map.length
    }

    getHeight = () => {
        return this.map.length * GameMap.TILE_HEIGHT
    }

    clear = () => {
    }

    edgeCheck = (row, column) => {
        row += this.topEdgeCheck()
        this.bottomEdgeCheck()
        column += this.leftEdgeCheck()
        this.rightEdgeCheck()
        this.cellCheck(row, column)
    }

    topEdgeCheck = () => {
        let i = 0
        let val = 0
        for (const cell of this.map[0]) {
            if (cell.owned !== false) {
                val = 1
                let newRow = []
                for (let j = 0; j < this.map[0].length; j++) {
                    newRow[j] = { texture: GameMap.TILE_VOID, owned: false }
                }
                this.map.unshift(newRow)
                this.map[0][i] = { texture: GameMap.TILE_VOID, owned: false }
            }
            i++
        }
        return val
    }

    leftEdgeCheck = () => {
        let i = 0
        let val = 0
        for (const row of this.map) {
            if (row[0].owned !== false) {
                val = 1
                const newCell = { texture: GameMap.TILE_VOID, owned: false }
                for (const rowA of this.map) {
                    rowA.unshift(JSON.parse(JSON.stringify(newCell)))
                }

                //this.map[i][0] = { texture: GameMap.TILE_VOID, owned: false }
                break
            }
            i++
        }
        return val
    }

    rightEdgeCheck = () => {
        let i = 0
        for (const row of this.map) {
            if (row[row.length-1].owned !== false) {
                const newCell = { texture: GameMap.TILE_VOID, owned: false }
                for (const rowA of this.map) {
                    rowA.push(JSON.parse(JSON.stringify(newCell)))
                }

                //this.map[i][row.length-1] = { texture: GameMap.TILE_VOID, owned: false}
                break
            }
            i++
        }
    }

    bottomEdgeCheck = () => {
        let i = 0
        for (const cell of this.map[this.map.length - 1]) {
            if (cell.owned !== false) {
                let newRow = []
                for (let j = 0; j < this.map[this.map.length - 1].length; j++) {
                    newRow[j] = { texture: GameMap.TILE_VOID, owned: false }
                }
                this.map.push(newRow)
                this.map[this.map.length - 1][i] = { texture: GameMap.TILE_VOID, owned: false}
            }
            i++
        }
    }

    cellCheck = (row, column) => {
        this.voidToGrass(row, column + 1)
        this.voidToGrass(row, column - 1)
        this.voidToGrass(row + 1, column)
        this.voidToGrass(row - 1, column)
    }

    voidToGrass = (row, column) => {
        if(this.map[row][column].texture === GameMap.TILE_VOID) {
            this.map[row][column].texture = GameMap.TILE_GRASS
            this.map[row][column].title = "Grass Tile"
        }
    }
}
