export let SceneSetting = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneSetting" })
                let resize = () => {
            if (this.sys.game.scene.isActive(this.scene.key)) {
                this.scene.restart()
            }
        }

        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function (gameSettings) {
        this.gameSettings = gameSettings
    },
    preload: () => { },
    create: function () {
        let soundOffButton = this.add.text(
            this.sys.game.canvas.width / 2 - 100,
            this.sys.game.canvas.height / 2,
            "Sound Off",
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold",
                backgroundColor: "#ff8888",
                padding: { x: 20, y: 20 },

            }
        )
            .setOrigin(0.5)
            .setInteractive({ useHandCursor: true })
            .on('pointerdown', () => {
                const soundOff = new Event('sound-off')
                window.dispatchEvent(soundOff)
            })

        let soundOnButton = this.add.text(
            this.sys.game.canvas.width / 2 + 100,
            this.sys.game.canvas.height / 2,
            "Sound On",
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold",
                backgroundColor: "#ff8888",
                padding: { x: 20, y: 20 },

            }
        )
            .setOrigin(0.5)
            .setInteractive({ useHandCursor: true })
            .on('pointerdown', () => {
                const soundOn = new Event('sound-on')
                window.dispatchEvent(soundOn)
            })


        let settingsButton = this.add.text(
            this.sys.game.canvas.width / 2,
            this.sys.game.canvas.height / 1.5,
            "Back",
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold",
                backgroundColor: "#8888ff",
                padding: { x: 10, y: 10 },

            }
        )
            .setOrigin(0.5)
            .setInteractive({ useHandCursor: true })
            .on('pointerdown', () => {
                this.scene.start("SceneMenu")
            })


    },
    update: () => { }
})