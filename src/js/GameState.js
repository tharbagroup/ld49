import { Chicken } from './animals/Chicken.js'
import { Cow } from './animals/Cow.js'
import { Rabbit } from './animals/Rabbit.js'
import { GameMap } from './GameMap.js'

export class GameState {
    constructor(playerName, townName)
    {
        this.playerName = playerName
        this.townName = townName
        this.population = 2
        this.day = 1
        this.processedDay = 0
        this.hour = 1
        this.timeSpeed = 1
        this.paused = false
        this.animalMenu = false
        this.money = 100
        this.meat = 25
        this.grass = 50
        this.recruitChance = 5
        this.animalChance = 5
        this.animals = {
            Chicken: [new Chicken({animalAge: Math.floor(Math.random()* 10)}), new Chicken({animalAge: Math.floor(Math.random()* 10)}), new Chicken({animalAge: Math.floor(Math.random()* 10)}), new Chicken({animalAge: Math.floor(Math.random()* 10)})],
            Cow: [new Cow({animalAge: Math.floor(Math.random()* 15)}), new Cow({animalAge: Math.floor(Math.random()* 15)})],
            Rabbit: [new Rabbit({animalAge: Math.floor(Math.random()* 5)}), new Rabbit({animalAge: Math.floor(Math.random()* 5)}), new Rabbit({animalAge: Math.floor(Math.random()* 5)}), new Rabbit({animalAge: Math.floor(Math.random()* 5)})]

        }
        this.mapdata = new GameMap()
    }
}