import { SceneAnimal } from './SceneAnimal.js'
export let SceneUI = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneUI" })
        let resize = () => {
            if (this.sys.game.scene.isActive(this.scene.key)) {
                this.scene.restart()
            }
        }

        window.addEventListener("resize", resize.bind(this), false);

    },
    init: function (gameState) {
        this.gameState = gameState
    },
    preload: function () {
        this.load.spritesheet('uiBorder', 'images/ui-border.png', {
            frameWidth: 32,
            frameHeight: 32
        })
    },

    create: function () {

        const sceneDay = this.scene.get('SceneDay')
        let sceneAnimalMenu
        const topUIBackground = this.add.tileSprite(0, 32, this.sys.game.canvas.width, 32, 'uiBorder', 0)
            .setScrollFactor(0)
            .setDepth(11)
            .setScale(2)

        const bottomUIBackground = this.add.tileSprite(0, this.sys.game.canvas.height - 32, this.sys.game.canvas.width, 32, 'uiBorder', 2)
            .setScrollFactor(0)
            .setDepth(11)
            .setScale(2)

        const leftUIBackground = this.add.tileSprite(0, 0, 64, this.sys.game.canvas.height, 'uiBorder', 3)
            .setScrollFactor(0)
            .setDepth(10)
            .setScale(2)


        const rightUIBackground = this.add.tileSprite(this.sys.game.canvas.width - 32, 0, 32, this.sys.game.canvas.height, 'uiBorder', 1)
            .setScrollFactor(0)
            .setDepth(10)
            .setScale(2)

        /**
         * Top Bar UI
         */

        const menuText = this.add.text(
            this.sys.game.canvas.width - 16,
            24,
            `Morning, Day ${this.gameState.day}`,
            {
                fontSize: 32,
                color: "#FF0000",
                fontStyle: "bold"
            }
        ).setOrigin(1, 0.5)
            .setScrollFactor(0, 0)
            .setDepth(12)

        sceneDay.events.on('updateTime', (time) => {
            menuText.setText(`${time}, Day ${this.gameState.day}`)
            menuText.update()
        })


        const speedText = this.add.text(this.sys.game.canvas.width - 32 - menuText.displayWidth,
            24,
            `${this.gameState.timeSpeed}X Speed`,
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold"
            }
        ).setOrigin(1, 0.5)
            .setScrollFactor(0, 0)
            .setDepth(12)

        /**
         * Bottom Bar UI
         */
        let uiOffset = 16
        const inventoryCoinIcon = this.add.sprite(uiOffset, this.sys.game.canvas.height - 24, 'currencies', '0')
            .setDepth(12)
            .setScrollFactor(0, 0)
        uiOffset += inventoryCoinIcon.width
        const inventoryCoinText = this.add.text(uiOffset, this.sys.game.canvas.height - 24, `${this.gameState.money}`, {
            fontSize: 24,
            color: "#FFFFFF",
            fontStyle: "bold"
        }
        )
            .setDepth(12)
            .setOrigin(0.5)
            .setScrollFactor(0, 0)
        uiOffset += inventoryCoinText.width + 8
        const inventoryMeatIcon = this.add.sprite(uiOffset, this.sys.game.canvas.height - 24, 'currencies', '1')
            .setDepth(12)
            .setScrollFactor(0, 0)
        uiOffset += inventoryMeatIcon.width

        const inventoryMeatText = this.add.text(uiOffset, this.sys.game.canvas.height - 24, `${this.gameState.meat}`, {
            fontSize: 24,
            color: "#FFFFFF",
            fontStyle: "bold"
        }
        )
            .setDepth(12)
            .setOrigin(0.5)
            .setScrollFactor(0, 0)
        uiOffset += inventoryMeatText.width + 8
        const inventoryGrassIcon = this.add.sprite(uiOffset, this.sys.game.canvas.height - 24, 'currencies', '2')
            .setDepth(12)
            .setScrollFactor(0, 0)
        uiOffset += inventoryGrassIcon.width

        const inventoryGrassText = this.add.text(uiOffset, this.sys.game.canvas.height - 24, `${this.gameState.grass}`, {
            fontSize: 24,
            color: "#FFFFFF",
            fontStyle: "bold"
        }
        )
            .setDepth(12)
            .setOrigin(0.5)
            .setScrollFactor(0, 0)
        uiOffset += inventoryGrassText.width + 8

        /**
         * Button Bar UI
         */
        let bottomUIOffset = 16

        const inventoryPeopleText = this.add.text(this.sys.game.canvas.width - bottomUIOffset, this.sys.game.canvas.height - 24, `${this.gameState.population}`, {
            fontSize: 24,
            color: "#FFFFFF",
            fontStyle: "bold"
        }
        )
            .setDepth(12)
            .setOrigin(1, 0.5)
            .setScrollFactor(0, 0)
        bottomUIOffset += inventoryPeopleText.width + 8

        const inventoryPeopleIcon = this.add.sprite(this.sys.game.canvas.width - bottomUIOffset, this.sys.game.canvas.height - 24, 'currencies', '3')
            .setDepth(12)
            .setOrigin(1, 0.5)
            .setScrollFactor(0, 0)
        bottomUIOffset += inventoryPeopleIcon.width

        /**
         * Left Bar UI
         */

        let leftUIOffset = 108

        let clearAnimalMenu = () => {
            sceneAnimalMenu.events.off('close')
            this.scene.remove('SceneAnimal')

            sceneAnimalMenu = null
            if (this.gameState.timeSpeed === 0) {
                speedText.setText("Paused")
            } else {
                speedText.setText(`${this.gameState.timeSpeed}X Speed`)
            }
            speedText.update()
            this.gameState.paused = false
            this.events.emit('resume')
        }


        const inventoryChickenIcon = this.add.sprite(24, leftUIOffset, 'animals', '0')
            .setDepth(12)
            .setOrigin(0.5, 0.0)
            .setScrollFactor(0, 0)
            .setInteractive({ useHandCursor: true })

        leftUIOffset += inventoryChickenIcon.height
        const inventoryChickenText = this.add.text(24, leftUIOffset, `${this.gameState.animals.Chicken.length}`, {
            fontSize: 24,
            color: "#FFFFFF",
            fontStyle: "bold"
        }
        )
            .setDepth(12)
            .setOrigin(0.5, 0)
            .setScrollFactor(0, 0)
        leftUIOffset += inventoryChickenText.height + 8



        const inventoryCowIcon = this.add.sprite(24, leftUIOffset, 'animals', '1')
            .setDepth(12)
            .setOrigin(0.5, 0.0)
            .setScrollFactor(0, 0)
            .setInteractive({ useHandCursor: true })
        leftUIOffset += inventoryCowIcon.height
        const inventoryCowText = this.add.text(24, leftUIOffset, `${this.gameState.animals.Cow.length}`, {
            fontSize: 24,
            color: "#FFFFFF",
            fontStyle: "bold"
        }
        )
            .setDepth(12)
            .setOrigin(0.5, 0)
            .setScrollFactor(0, 0)
        leftUIOffset += inventoryCowText.height + 8


        const inventoryRabbitIcon = this.add.sprite(24, leftUIOffset, 'animals', '2')
            .setDepth(12)
            .setOrigin(0.5, 0.0)
            .setScrollFactor(0, 0)
            .setInteractive({ useHandCursor: true })
        leftUIOffset += inventoryRabbitIcon.height
        const inventoryRabbitText = this.add.text(24, leftUIOffset, `${this.gameState.animals.Rabbit.length}`, {
            fontSize: 24,
            color: "#FFFFFF",
            fontStyle: "bold"
        }
        )
            .setDepth(12)
            .setOrigin(0.5, 0)
            .setScrollFactor(0, 0)
        leftUIOffset += inventoryRabbitText.height + 8


        let updateCurrency = () => {
            inventoryCoinText.setText(`${this.gameState.money}`)
            inventoryCoinText.update()
            inventoryMeatText.setText(`${this.gameState.meat}`)
            inventoryCoinText.update()
            inventoryGrassText.setText(`${this.gameState.grass}`)
            inventoryGrassText.update()
        }

        let updateAnimals = () => {
            inventoryChickenText.setText(`${this.gameState.animals.Chicken.length}`)
            inventoryChickenText.update()
            inventoryCowText.setText(`${this.gameState.animals.Cow.length}`)
            inventoryCowText.update()
            inventoryRabbitText.setText(`${this.gameState.animals.Rabbit.length}`)
            inventoryRabbitText.update()
        }

        inventoryChickenIcon.on('pointerdown', () => {
            if (this.gameState.paused !== true) {
                this.events.emit('pause')
                this.gameState.paused = true
                speedText.setText("Paused")
                speedText.update()
                this.scene.add('SceneAnimal', SceneAnimal)
                this.scene.launch('SceneAnimal', { gameState: this.gameState, animal: 'Chicken' })
                sceneAnimalMenu = this.scene.get('SceneAnimal')
                sceneAnimalMenu.events.on('close', () => {
                    clearAnimalMenu()
                })
                sceneAnimalMenu.events.on('updateCurrency', () => {
                    updateCurrency()
                })
                sceneAnimalMenu.events.on('updateAnimals', () => {
                    updateAnimals()
                })
                this.scene.bringToTop('SceneAnimal')
            }
        })

        inventoryCowIcon.on('pointerdown', () => {
            if (this.gameState.paused !== true) {
                this.events.emit('pause')
                this.gameState.paused = true
                speedText.setText("Paused")
                speedText.update()
                this.scene.add('SceneAnimal', SceneAnimal)
                this.scene.launch('SceneAnimal', { gameState: this.gameState, animal: 'Cow' })
                sceneAnimalMenu = this.scene.get('SceneAnimal')
                sceneAnimalMenu.events.on('close', () => {
                    clearAnimalMenu()
                })
                sceneAnimalMenu.events.on('updateCurrency', () => {
                    updateCurrency()
                })
                sceneAnimalMenu.events.on('updateAnimals', () => {
                    updateAnimals()
                })
                this.scene.bringToTop('SceneAnimal')
            }
        })

        inventoryRabbitIcon.on('pointerdown', () => {
            if (this.gameState.paused !== true) {
                this.events.emit('pause')
                this.gameState.paused = true
                speedText.setText("Paused")
                speedText.update()
                this.scene.add('SceneAnimal', SceneAnimal)
                this.scene.launch('SceneAnimal', { gameState: this.gameState, animal: 'Rabbit' })
                sceneAnimalMenu = this.scene.get('SceneAnimal')
                sceneAnimalMenu.events.on('close', () => {
                    clearAnimalMenu()
                })
                sceneAnimalMenu.events.on('updateCurrency', () => {
                    updateCurrency()
                })
                sceneAnimalMenu.events.on('updateAnimals', () => {
                    updateAnimals()
                })
                this.scene.bringToTop('SceneAnimal')
            }
        })


        let townType
        if (this.gameState.population > 200) {
            townType = "Megacity"
        } else if (this.gameState.population > 150) {
            townType = "Municipality"
        } else if (this.gameState.population > 125) {
            townType = "City"
        } else if (this.gameState.population > 100) {
            townType = "County"
        } else if (this.gameState.population > 80) {
            townType = "District"
        } else if (this.gameState.population > 60) {
            townType = "Town"
        } else if (this.gameState.population > 50) {
            townType = "Township"
        } else if (this.gameState.population > 40) {
            townType = "Subdistrict"
        } else if (this.gameState.population > 30) {
            townType = "Locality"
        } else if (this.gameState.population > 20) {
            townType = "Village"
        } else if (this.gameState.population > 10) {
            townType = "Hamlet"
        } else if (this.gameState.population > 5) {
            townType = "Roadhouse"
        } else {
            townType = "Farm"
        }

        const townText = this.add.text(16,
            24,
            `${townType} of ${this.gameState.townName}`,
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold"
            }
        ).setOrigin(0, 0.5)
            .setScrollFactor(0, 0)
            .setDepth(12)

        sceneDay.events.on('updateTimeLoop', (time) => {
            if (time === 0) {
                speedText.setText("Paused")
            } else {
                speedText.setText(`${time}X Speed`)
            }
            this.gameState.timeSpeed = time
            speedText.update()
        })

        let shutdown = () => {
            sceneDay.events.off('updateTime');
            sceneDay.events.off('updateTimeLoop');
        }
        this.events.on('shutdown', shutdown, this);

    },
    update: function () { }
})
