import { SceneMenu } from './SceneMenu.js'
import { SceneSetting } from './SceneSetting.js'
import { SceneStartGame } from './SceneStartGame.js'
import { SceneManageDay } from './SceneManageDay.js'
import { SceneDay } from './SceneDay.js'
import { SceneUI } from './SceneUI.js'
import { SceneBackground } from './SceneBackground.js'
import { SceneAnimal } from './SceneAnimal.js'


const phaserSettings = {
    type: Phaser.AUTO,
    parent: "game",
    width: window.innerWidth,
    height: window.innerHeight,
    backgroundColor: "#000000",
    scene: [ SceneMenu, SceneSetting, SceneStartGame, SceneManageDay, SceneDay, SceneUI, SceneBackground],
    dom: {
        createContainer: true
    },
    pixelArt: true
}

const game = new Phaser.Game(phaserSettings)

window.addEventListener('resize', () => {
    game.scale.resize(window.innerWidth, window.innerHeight)
})

const backgroundSound = new Audio('./sound/background.wav')
backgroundSound.addEventListener('ended', function() {
    this.currentTime = 0
    this.play()
}, false)
window.addEventListener('focus', () => {
    backgroundSound.play()
})


window.addEventListener('blur', () => {
    backgroundSound.pause()
})

window.addEventListener('sound-off', () => {
    backgroundSound.volume = 0
})

window.addEventListener('sound-on', () => {
    backgroundSound.volume = 1
})